import React from 'react';
import Navigation from "../conponents/nav";
import Table from "../conponents/table";
import Footer from "../conponents/footer";

const Home = () => {
    return (
        <div>
            <Navigation />
            <Table />
            <Footer />
        </div>
    );
};

export default Home;